with Ada.Containers.Generic_Array_Sort, Ada.Containers.Generic_Sort;
with Ada.Strings.Unbounded;
with Ada.Text_IO;
with Options, Scanner, History;

procedure xdxd is

	use Ada.Text_IO, Ada.Strings.Unbounded;
	opts: Options.Option_Values;

	Invalid_Choice: exception;

	procedure Present_Results(Selection: out Unbounded_String) is
		results: History.Path_Array := Scanner.Matches;
		subtype Result_Index is Positive range results'Range;
		counts: History.Reference_Count_Array (Result_Index) := (others => 0);
		type Sorting_Index is new Positive range results'Range;
		sorted: array (Sorting_Index) of Result_Index;

		function Counts_And_Path_Before(Left, Right: in Sorting_Index) return Boolean is
			L: Result_Index renames sorted(Left);
			R: Result_Index renames sorted(Right);
		begin
			if counts(L) > counts(R) then return true;
			elsif counts(L) = counts(R) then return results(L) < results(R);
			else return false;
			end if;
		end Counts_And_Path_Before;

		procedure Swap(Left, Right: in Sorting_Index) is
			T: Natural := sorted(Left);
		begin
			sorted(Left) := sorted(Right);
			sorted(Right) := T;
		end Swap;

		procedure Sort_By_Counts_And_Path is new Ada.Containers.Generic_Sort (Sorting_Index, Counts_And_Path_Before, Swap);
		procedure Sort_Results is new Ada.Containers.Generic_Array_Sort (Positive, Unbounded_String, History.Path_Array);

		function Option_Name(I: in Natural) return Character is
			(case I is
				when 0 .. 9 => Character'Val(Character'Pos('0') + I),
				when 10 .. 36 => Character'Val(Character'Pos('a') + I - 10),
				when others => '?');

		function Choice_Val(C: in Character) return Positive is
			(case C is
				when '1' .. '9' => Character'Pos(C) - Character'Pos('1') + 1,
				when 'a' .. 'z' => Character'Pos(C) - Character'Pos('a') + 10,
				when 'A' .. 'Z' => Character'Pos(C) - Character'Pos('Z') + 10,
				when others => raise Invalid_Choice);

	begin
		if results'Length = 1 then
			Selection := results(1);
		elsif results'Length = 0 then
			Put_Line(Current_Error, "No Solutions");
			Selection := To_Unbounded_String(".");
		else
			Sort_Results(results);
			History.Read_Counts(To_String(Opts.history), results, counts);
			for I in sorted'Range loop sorted(I) := Result_Index(I); end loop;
			Sort_By_Counts_And_Path(sorted'First, sorted'Last);
			for I in sorted'Range loop
				Put_Line(Current_Error, ' ' & Option_Name(Natural(I)) & ": " & To_String(results(sorted(I))));
			end loop;
			declare
				choice: Character;
				ix: Sorting_Index;
			begin
				Get_Immediate(choice);
				ix := Sorting_Index(Choice_Val(choice));
				Selection := results(sorted(ix));
			exception
				when Constraint_Error => raise Invalid_Choice; -- not in range of Sorting_Index
			end;
		end if;
	end Present_Results;

	procedure Print_Version is begin
		Put_Line("xdxd 0.1");
	end Print_Version;

begin
	declare
		first_opts: Options.Option_Values;
		parameter: String := Options.Parse_Command_Line(Opts => first_opts);
	begin
		if first_opts.print_version then Print_Version; return; end if;
		Options.Read_Rc(To_String(first_opts.config_file), Opts => opts);
		declare
			redundant: String := Options.Parse_Command_Line(Opts => opts);
			pragma Assert (redundant = parameter);
		begin
			Scanner.Run(opts, parameter);
		end;
	end;
	declare pick: Unbounded_String; begin
		Present_Results(Selection => pick);
		if pick /= Null_Unbounded_String then
			Put_Line(To_String(pick));
			History.Bump_Count(To_String(Opts.history), To_String(pick));
		end if;
	exception
		when Invalid_Choice | End_Error => null;
	end;
end xdxd;
