package body Binary_Search is

	function Binary_Search(Haystack: in Array_Type; Needle: in Key_Type) return Index_Type is
		function Midpoint(A, B: in Index_Type) return Index_Type
			is (Index_Type((Long_Integer(A) + Long_Integer(B)) / 2));
		low, mid, high: Index_Type;
	begin
		low := Haystack'First; high := Haystack'Last + 1;
		while low < high loop
			mid := Midpoint(low, high);
			if Haystack(mid) < Needle then low := mid + 1;
			elsif Haystack(mid) = Needle then return mid;
			else high := mid;
			end if;
		end loop;
		raise Not_Found;
	end Binary_Search;

end Binary_Search;
