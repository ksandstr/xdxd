#!/bin/sh
exec prove -v -m `test -e t/*.c && echo t/*.c | sed 's/\.c\( \|$\)/ /g'` t/*.t
