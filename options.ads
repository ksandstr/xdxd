with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Options is

	type Root_Mode is (Always, If_Empty, Never);
	type Dir_Mode is (Symlinks, Unique);

	subtype Path_Length is Natural range 0 .. 4096;
	type Ignore_Type; type Ignore_Ptr is access all Ignore_Type;
	type Ignore_Type (Length: Path_Length) is
		record
			next: aliased Ignore_Ptr;
			path: String (1 .. Length);
		end record;
	type Option_Values is
		record
			add_root: Root_Mode := Never;
			ignore_list: aliased Ignore_Ptr;
			directories: Dir_Mode := Symlinks;
			generalized_search: Boolean;
			history, config_file: Unbounded_String;
			homedir_char: Character := '.';
			verbose, print_version, no_ignore, traditional, icase: Boolean := false;
		end record;

	procedure Read_Rc(Config_File: in String; Opts: out Option_Values);
	function Parse_Command_Line(Opts: out Option_Values) return String;

end Options;
