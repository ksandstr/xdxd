#!/bin/sh
for spec in *.ads; do
	body=`echo $spec | sed 's/\.ads$/.adb/'`
	if ! [ -f $body ]; then
		echo ": $spec |> !gccada |>"
	fi
done
