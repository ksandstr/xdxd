with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Fixed, Ada.Strings.Maps;
with Ada.Environment_Variables, Ada.Command_Line;
with Binary_Search;

package body Options is

	use Ada.Strings, Ada.Strings.Fixed, Ada.Strings.Maps;
	package Env renames Ada.Environment_Variables;

	procedure Add_Root(Rest: in String; Opts: out Option_Values) is
		mode: String renames Rest;
	begin
		if mode = "always" then Opts.add_root := Always;
		elsif mode = "if-empty" then Opts.add_root := If_Empty;
		elsif mode = "never" then Opts.add_root := Never;
		else Put_Line(Current_Error, "xdxd: ignoring unknown mode `" & mode & "' in `add-root " & Rest & "'");
		end if;
	end Add_Root;

	procedure All_Option(Rest: in String; Opts: out Option_Values) is begin
		if Rest /= "" then Put_Line(Current_Error, "option value `" & Rest & "' ignored"); end if;
		Opts.no_ignore := true;
	end All_Option;

	procedure Ignore(Rest: in String; Opts: out Option_Values) is
		tail: access Ignore_Ptr := Opts.ignore_list'Access;
	begin
		while tail.all /= null loop tail := tail.all.next'Access; end loop;
		tail.all := new Ignore_Type'(Length => Rest'Length, path => Rest, next => null);
	end Ignore;

	procedure Generalized_Search(Rest: in String; Opts: out Option_Values) is begin
		Opts.generalized_search := true;
	end Generalized_Search;

	procedure Verbose(Rest: in String; Opts: out Option_Values) is begin
		Opts.verbose := true;
	end Verbose;

	procedure Version(Rest: in String; Opts: out Option_Values) is begin
		Opts.print_version := true;
	end Version;

	procedure Config_File(Rest: in String; Opts: out Option_Values) is
		str: constant String := (if Rest(Rest'First) = '~' then Env.Value("HOME") & Rest(Rest'First + 1 .. Rest'Last) else Rest);
	begin
		Opts.config_File := To_Unbounded_String(str);
	end Config_File;

	procedure History(Rest: in String; Opts: out Option_Values) is begin
		Opts.history := To_Unbounded_String(Rest);
	end History;

	procedure Homedir_Char(Rest: in String; Opts: out Option_Values) is begin
		if Rest'Length > 1 then Put_Line("homedir-char option `" & Rest & "' too long; extra letters ignored"); end if;
		Opts.homedir_char := Rest(Rest'First);
	end Homedir_Char;

	procedure Icase(Rest: in String; Opts: out Option_Values) is begin
		Opts.icase := true;
	end Icase;

	procedure Traditional(Rest: in String; Opts: out Option_Values) is begin
		Opts.traditional := true;
	end Traditional;

	-- TODO
	procedure Directories(Rest: in String; Opts: out Option_Values) is null;
	procedure Help(Rest: in String; Opts: out Option_Values) is null;
	procedure History_Lifetime(Rest: in String; Opts: out Option_Values) is null;
	procedure History_Position(Rest: in String; Opts: out Option_Values) is null;
	procedure History_Maxsize(Rest: in String; Opts: out Option_Values) is null;
	procedure History_Separate(Rest: in String; Opts: out Option_Values) is null;
	procedure Start_At(Rest: in String; Opts: out Option_Values) is null;

	type Option_Proc is access procedure (Rest: in String; Opts: out Option_Values);
	subtype Option_Name is String (1 .. 18);
	type Option is
		record
			name: Option_Name;
			proc: Option_Proc;
			cmd_only: Boolean := false;
		end record;
	type Option_Array is array (Positive range <>) of Option;

	commands: constant Option_Array (1 .. 16) :=
		( ("add-root          ", Add_Root'Access, false),
		  ("config-file       ", Config_File'Access, true),
		  ("directories       ", Directories'Access, false),
		  ("generalized-search", Generalized_Search'Access, false),
		  ("history           ", History'Access, false),
		  ("history-lifetime  ", History_Lifetime'Access, false),
		  ("history-maxsize   ", History_Maxsize'Access, false),
		  ("history-position  ", History_Position'Access, false),
		  ("history-separate  ", History_Separate'Access, false),
		  ("homedir-char      ", Homedir_Char'Access, false),
		  ("icase             ", Icase'Access, false),
		  ("ignore            ", Ignore'Access, false),
		  ("start-at          ", Start_At'Access, false),
		  ("traditional       ", Traditional'Access, false),
		  ("verbose           ", Verbose'Access, true),
		  ("version           ", Version'Access, true));
	pragma Assert (for all I in commands'First + 1 .. commands'Last => commands(I - 1).name < commands(I).name);

	function "<"(Left: in Option; Right: in Option_Name) return Boolean is (Left.name < Right);
	function "="(Left: in Option; Right: in Option_Name) return Boolean is (Left.name = Right);
	package FO is new Binary_Search (Positive, Option, Option_Array, Option_Name, "<", "=");
	Option_Not_Found: exception renames FO.Not_Found;
	function Find_Option(Haystack: in Option_Array; Needle: in String) return Positive renames FO.Binary_Search;

	procedure Read_Rc(Config_File: in String; Opts: out Option_Values) is
		input: File_Type;
	begin
		Open(input, In_File, (if Config_File /= "" then Config_File else Env.Value("HOME") & "/.xdrc"));
		while not End_Of_File(input) loop
			declare
				line: String := Get_Line(input);
				space: Natural := Index(line, To_Set(" "), 1);
				command: String := (if space > 0 then Head(line, space - 1) else line);
				rest: String renames Trim(line(space + 1 .. line'Last), Left);
				ix: Positive;
			begin
				ix := Find_Option(commands, Head(command, Option_Name'Length));
				if not commands(ix).cmd_only then
					commands(ix).proc(rest, Opts);
				else
					Put_Line(Current_Error, "xdrc directive `" & Trim(commands(ix).name, Right) & "' cannot appear in xdrc file; ignored");
				end if;
			exception
				when Option_Not_Found => Put_Line(Current_Error, "xdrc directive `" & command & "' not understood");
				when Index_Error => Put_Line("malformed option line `" & line & "'"); -- TODO: print line number?
			end;
		end loop;
		Close(input);
	exception
		when Name_Error => null;
	end Read_Rc;

	type Short is
		record
			char: Character;
			proc: Option_Proc;
			has_arg: Boolean;
		end record;
	type Short_Option_Array is array (Positive range <>) of Short;

	short_options: constant Short_Option_Array (1 .. 7) :=
		( ('V', Verbose'Access, false),
		  ('a', All_Option'Access, false),
		  ('c', Config_File'Access, true),
		  ('g', Generalized_Search'Access, false),
		  ('h', Help'Access, false),
		  ('i', Icase'Access, false),
		  ('v', Version'Access, false));
	pragma Assert (for all I in short_options'First + 1 .. short_options'Last => short_options(I - 1).char < short_options(I).char);

	function "<"(Left: in Short; Right: in Character) return Boolean is (Left.char < Right);
	function "="(Left: in Short; Right: in Character) return Boolean is (Left.char = Right);
	package FSO is new Binary_Search (Positive, Short, Short_Option_Array, Character, "<", "=");
	Short_Not_Found: exception renames FSO.Not_Found;
	function Find_Short(Haystack: in Short_Option_Array; Needle: in Character) return Positive renames FSO.Binary_Search;

	-- TODO: most of this is woefully undertested. it should be in a proper
	-- module, like an Util.Command_Line_Options or some such.
	function Parse_Command_Line(Opts: out Option_Values) return String is
		package Cmd renames Ada.Command_Line;
		result: Unbounded_String;
		I: Positive := 1;
		dashes: Boolean := false;
		Invalid: exception;
	begin
		while I <= Cmd.Argument_Count loop
			declare arg: constant String := Cmd.Argument(I); begin
				if dashes or else (arg'Length >= 1 and then arg(arg'First) /= '-') then -- not an option
					if result /= Null_Unbounded_String then
						Put_Line(Current_Error, "extra argument `" & arg & "' ignored");
					else
						result := To_Unbounded_String(arg);
					end if;
				elsif arg'Length >= 2 and then arg(arg'First + 1) /= '-' then -- short option
					if arg'Length > 2 then raise Invalid; end if;
					declare opt: constant Short := short_options(Find_Short(short_options, arg(arg'First + 1))); begin
						if not opt.has_arg then opt.proc("", Opts);
						else
							I := I + 1;
							opt.proc((if I <= Cmd.Argument_Count then Cmd.Argument(I) else ""), Opts);
						end if;
					end;
				elsif arg'Length = 2 then -- the double-dash
					pragma Assert (arg = "--");
					dashes := true;
				else -- long option
					declare
						sep: Natural := Index(arg, "=");
						name: String := Head(arg(arg'First + 2 .. (if sep = 0 then arg'Last else sep - 1)), commands(1).name'Length);
					begin
						if name'Length < (if sep > 0 then sep else arg'Length) then raise Invalid; end if;
						commands(Find_Option(commands, name)).proc((if sep > 0 then arg(sep + 1 .. arg'Last) else ""), Opts);
					end;
				end if;
			exception
				when Invalid => Put_Line(Current_Error, "invalid argument `" & Cmd.Argument(I) & "'");
				when Short_Not_Found | Option_Not_Found => Put_Line(Current_Error, "option `" & Cmd.Argument(I) & "' not found");
			end;
			I := I + 1;
		end loop;
		if Opts.traditional then Opts.generalized_search := false; end if;
		return To_String(result);
	end Parse_Command_Line;

end Options;
