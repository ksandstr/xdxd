package XdxdUtils;
require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(run_xdxd);
use strict;
use FindBin qw($Bin);
use Test::More;

my $refprg = '/usr/bin/xd';
$refprg = '/usr/local/bin/xd' unless -x $refprg;
if(!-x $refprg) { diag("?refprg not found"); undef $refprg; }
my $testprg;
for (qw/build-devel build-release ./) {
	my $p = "$Bin/../$_/xdxd";
	if(-x $p) {
		$testprg = $p;
		last;
	}
}

sub run_xdxd {
	my %args = @_;
	local $ENV{HOME} = $args{HOME} // "$Bin";
	chomp(my $main = `$testprg $args{args} 2>&1`);
	$main =~ s/\/$//;
	SKIP: {
		skip "no refprg", 1 unless $refprg;
		chomp(my $ref = `$refprg $args{args} 2>&1`);
		$ref = "No Solutions" if $ref =~ /^No Solutions/; # what in tarnation
		$ref =~ s/\/$//;
		is($main, $ref, "ref case for `xd $args{args}'");
	}
	return $main;
}

1;
