#!/usr/bin/perl
use strict;
use FindBin qw($Bin);
use File::Temp;
use Test::More tests => 8;
use lib "$Bin/perl";
use XdxdUtils qw/run_xdxd/;

# very basic test: create $temp/a/b/c, then query with "xd abc" with HOME=$temp.
my $temp = File::Temp->newdir();
mkdir("$temp/a");
mkdir("$temp/a/b");
mkdir("$temp/a/b/c");

my $main = run_xdxd(HOME => $temp, args => "abc");
is($main, "$temp/a/b/c", "xd abc yields \$temp/a/b/c");

# do --icase as a bonus, and to reduce bulk of subsequent tests.
$main = run_xdxd(HOME => $temp, args => "-g ABC");
is($main, "No Solutions", "xd -g ABC yields nothing");
$main = run_xdxd(HOME => $temp, args => "--icase -g ABC");
is($main, "$temp/a/b/c", "xd --icase -g ABC yields \$temp/a/b/c");
$main = run_xdxd(HOME => $temp, args => "-i -g ABC");
is($main, "$temp/a/b/c", "xd -i -g ABC yields \$temp/a/b/c");
