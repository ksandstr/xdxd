#!/usr/bin/perl
use strict;
use v5.10;
use FindBin qw($Bin);
use IO::File;
use File::Temp;
use Test::More tests => 8;
use lib "$Bin/perl";
use XdxdUtils qw/run_xdxd/;

# test that program behaviour is influenced by presence of a .xdrc file.

my $temp = File::Temp->newdir();
mkdir("$temp/a");
mkdir("$temp/a/b");
mkdir("$temp/a/b/c");

sub ate_xdrc {
	my $args = shift;
	my $out = run_xdxd(HOME => $temp, args => $args . " ABC");
	return $out eq "$temp/a/b/c";
}

ok(!ate_xdrc(""), "no .xdrc");

my $xdrc = IO::File->new("> $temp/.xdrc") or die "can't open `$temp/.xdrc: $!";
say $xdrc "icase";
say $xdrc "generalized-search";
$xdrc->close;
note("$temp/.xdrc added");
ok(ate_xdrc(""), "default .xdrc");
my $alt = "$temp/.alternative-xdrc";
rename("$temp/.xdrc", $alt) or die "rename failed: $!";
note("$temp/.xdrc moved to $alt");
ok(ate_xdrc("-c $temp/.alternative-xdrc"), "-c \$temp/.alternative-xdrc");
ok(ate_xdrc("--config-file=$temp/.alternative-xdrc"), "--config-file=\$temp/.alternative-xdrc");
