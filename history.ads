with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package History is

	type Path_Array is array (Positive range <>) of Unbounded_String;
	type Reference_Count_Array is array (Positive range <>) of Natural;

	procedure Read_Counts(History_Path: in String; Paths: in Path_Array; Counts: out Reference_Count_Array);
	procedure Bump_Count(History_Path: in String; Path: in String);

end History;
