# What

This is `xdxd`, a reimplementation of `Xd`, a fast directory changer. Licensed
under the GNU General Public License version 3-or-later, as found in `COPYING`.

## Why

The original `Xd` is a C++ program written against its author's personal
utility library, both of which compile using the author's personal build
system, all of which date from the nineties. As such their aggregate rot had
got up to the point where it was easier (and more interesting) to cold-up
reimplement those parts of the original which I was using, rather than deal
with porting its dependencies to systems where `Xd` wasn't already packaged
i.e. those not derived from Debian GNU/Linux.

## Is it any good?

Somewhat, but not all the way. It does provide the minimal generalized-search
function, and runs measurably quicker than `Xd` at that, but features such as
essentially every config and command-line option are broken, missing, or
undertested. Someone else can fix those, or not; previous users of `Xd` may
run into the same build difficulties I did, waddle on over, and discover their
favourite feature missing.

There are also a bunch of improvements that could be made to the central
scanning algorithm, such as a reduction in redundant results for matches
longer than a single character, and better Unicode support; but those were out
of my itch zone so I'm certainly not inspired to address them.

## Can I patch this? Will you laugh at me?

Oh sure, send 'em my way, I won't promise I'll laugh. But don't auto-reindent
the entire source even if an IDE tells you to, that's just _rude_. Nor is it
nice to write like the next guy also relied on IDE autocompletion.

### Oh help me RNGesus, it's in Ada

Yeah, "the language of death". Have a squiz though, it ain't half bad from a
contemporary perspective: there's very little punctuation, most of the
wordiness is well justified, and there are exactly three cases of explicit
memory allocation.

The choice of implementation language was due to Ada's reputation for low
maintenance, the fact that `Xd` can be cloned using only Ada's rigorously
specified standard libraries, and the historically low levels of unproven
wibble in the Ada standard revision process. These combined to make Ada 2012 a
very good option for a program I'd expect to be using for decades yet.

### What are the styling rules for this program?

Preferred line length is like 140 columns because ain't nobody stuck on 80 by
24 anymore. Feel free to exceed it up to 160 or so. Tabstop is 4, but use a
greater value locally according to preference. Always use a monospace font.

Hump\_Back\_Capitalize the names of single-letter variables, types,
subprograms, and packages, but leave longer variable names and record fields
lower case. Eschew vertical spacing and superfluous tabbing in favour of
making oneliners more significant; this enhances analyzability which is hugely
precious.

If these conventions gravely offend the eyes of your soul, adjust editor
settings until they no longer do.

### What's a "Tupfile"?

It's the input to a forgotten gold build system that doesn't, and cannot, work
on OpenBSD. (lol, OpenBSD.) Where inapplicable, build with `openbsd/build.sh`
instead.

## Okay.

Fine.

  -- Kalle A. Sandström <ksandstr@iki.fi>
