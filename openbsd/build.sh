#!/bin/sh

DEFAULT=xdxd
TARGET="$@"
test -z "$TARGET" && TARGET="$DEFAULT"

# keep these in sync with those in Tupfile and Tuprules.tup
ADAFLAGS="-gnat2012 -Wall -ffunction-sections -fdata-sections -fstack-check"
ADAFLAGS="$ADAFLAGS -gnatwK -gnatwF -gnatwJ"
if [ -z "$NDEBUG" ]; then
	ADAFLAGS="$ADAFLAGS -gnata -Og -g"
else
	ADAFLAGS="$ADAFLAGS -gnata -O2"
fi

GNATLINK_FLAGS="-Wl,--gc-sections"

exec gnatmake $MAKEFLAGS $ADAFLAGS $TARGET -largs $GNATLINK_FLAGS
