with Ada.Text_IO.Bounded_IO;
with Ada.Strings.Bounded, Ada.Strings.Fixed, Ada.Strings.Maps.Constants;
with Ada.Characters.Handling, Ada.Characters.Latin_1;
with Ada.Environment_Variables, Ada.Exceptions, Ada.Real_Time;
with GNAT.OS_Lib, Interfaces;
with Binary_Search;

package body History is

	use Ada.Exceptions;
	use Ada.Strings, Ada.Strings.Bounded, Ada.Strings.Fixed,
		Ada.Strings.Maps, Ada.Strings.Maps.Constants;
	use Ada.Text_IO;
	package Env renames Ada.Environment_Variables;
	package OS renames GNAT.OS_Lib;

	non_quoted: constant Character_Set := Alphanumeric_Set or To_Set("/-,._^");
	function Quote(Source: in String) return String is
		use Interfaces;
		buf: String(1 .. Source'Length * 4);
		O: Positive := 1;
		function Hex(V: in Unsigned_8) return String is
			type Hexits is array (Unsigned_8 range <>) of Character;
			table: constant Hexits (0 .. 15) := "0123456789abcdef";
		begin
			return table((V and 16#f0#) / 2**4) & table(V and 16#0f#);
		end Hex;
	begin
		for C of Source loop
			if Is_In(C, non_quoted) then
				buf(O) := C;
				O := O + 1;
			else
				buf(O .. O + 3) := "\x" & Hex(Character'Pos(C));
				O := O + 4;
			end if;
		end loop;
		return buf(buf'First .. Natural(O) - 1);
	end Quote;

	function Unhex(V: in String) return Character is
		use Ada.Characters.Handling;
		function Hexpos(C: in Character) return Natural is
			(case To_Lower(C) is
				when '0' .. '9' => Character'Pos(C) - Character'Pos('0'),
				when 'a' .. 'f' => Character'Pos(C) - Character'Pos('a') + 10,
				when others => raise Constraint_Error with "input `" & C & "' out of domain");
	begin
		pragma Assert (V'Length = 2);
		return Character'Val(Hexpos(V(V'First)) * 2**4 + Hexpos(V(V'First + 1)));
	exception
		when Constraint_Error => raise Constraint_Error with "invalid input `\x" & V & "'";
	end Unhex;

	function Unquote(Source: in String) return String is
		use Ada.Characters.Handling;
		buf: String(1 .. Source'Length);
		I: Natural := Source'First;
		O: Positive := buf'First;
	begin
		while I <= Source'Last loop
			if Source(I) = '\' then
				if I + 3 > Source'Last or else To_Lower(Source(I + 1)) /= 'x' then
					raise Constraint_Error with "not a hexit `" & Source(I .. Natural'Min(Source'Last, I + 3)) & "'";
				end if;
				buf(O) := Unhex(Source(I + 2 .. I + 3));
				I := I + 4;
			else
				buf(O) := Source(I);
				I := I + 1;
			end if;
			O := O + 1;
		end loop;
		return buf(buf'First .. Natural(O) - 1);
	end Unquote;

	subtype Path_Length is Natural range 0..4095;
	type History_Entry(Length: Path_Length) is
		record
			time: Long_Long_Integer;
			count: Natural;
			path: String(1 .. Length);
		end record;

	function Next_Entry(Input: in File_Type) return History_Entry is
		package Lines is new Generic_Bounded_Length (Max => 4096);
		package Lines_IO is new Ada.Text_IO.Bounded_IO (Lines);
		use Lines, Ada.Characters.Latin_1;
		line: Bounded_String := Lines_IO.Get_Line(Input);
		iter: Positive := 1;
		word_break: constant Character_Set := To_Set(' ') or To_Set(HT);
		function Next_Word return String is
			space: Natural := Index(line, word_break, iter);
		begin
			if space = 0 then
				if iter = Length(line) then raise End_Error with "history record has too few words"; end if;
				return ret: String := Slice(line, iter, Length(line)) do
					iter := Length(line);
				end return;
			else
				pragma Assert (iter < Length(line));
				return ret: String := Slice(line, iter, space - 1) do
					iter := space + 1;
				end return;
			end if;
		end Next_Word;
		time: Long_Long_Integer := Long_Long_Integer'Value(Next_Word);
		count: Natural := Natural'Value(Next_Word);
		path: String := Unquote(Trim(Next_Word, Left => word_break, Right => To_Set('/')));
	begin
		if path'Length = 0 or iter < Length(line) then raise End_Error with "broken history record"; end if;
		return History_Entry'(path'Length, time, count, path);
	end Next_Entry;

	function Default_History_Path return String is (Env.Value("HOME") & "/.xd_history");

	procedure Read_Counts(History_Path: in String; Paths: in Path_Array; Counts: out Reference_Count_Array) is
		file: File_Type;
		package BS is new Binary_Search (Positive, Unbounded_String, Path_Array, Unbounded_String);
	begin
		Open(file, In_File, (if History_Path = "" then Default_History_Path else History_Path));
		while not End_Of_File(file) loop
			declare E: History_Entry := Next_Entry(file); begin
				Counts(BS.Binary_Search(Paths, To_Unbounded_String(E.Path))) := E.count;
			exception
				when BS.Not_Found => null;
			end;
		end loop;
		Close(file);
	exception
		when Name_Error => null;
		when End_Error => Close(file); -- writeback will robustly unfuck the file, below
	end Read_Counts;

	function Seconds_Since_Epoch return Long_Long_Integer is
		use Ada.Real_Time;
		sec: Seconds_Count;
		fracs: Time_Span;
	begin
		Split(Clock, sec, fracs);
		return Long_Long_Integer(sec);
	end Seconds_Since_Epoch;

	procedure Write(To: in File_Type; E: in History_Entry) is
	begin
		Put_Line(To, Trim(E.time'Image, Left) & " " & Trim(E.count'Image, Left) & " " & Quote(E.path) & "/");
	end Write;

	-- TODO: set file mode to 0600
	procedure Create_History(History_Path: in String; First_Path: in String) is
		file: File_Type;
	begin
		Create(file, Out_File, History_Path);
		Write(file, History_Entry'(Length => First_Path'Length, time => Seconds_Since_Epoch, count => 1, path => First_Path));
		Close(file);
	end Create_History;

	Rename_Error: exception;
	procedure Substitute(From, To: in String) is
		ok: Boolean;
	begin
		OS.Rename_File(Old_Name => From, New_Name => To, Success => ok);
		if not ok then raise Rename_Error; end if;
	end Substitute;

	-- TODO: this is insufficiently robust against failure at various stages. it
	-- should at least clean up the temporary file on failure.
	procedure Bump_Count(History_Path: in String; Path: in String) is
		input, output: File_Type;
		hist_name: String := (if History_Path = "" then Default_History_Path else History_Path);
		temp_name: String := hist_name & ".xdxdtmp-" & Trim(OS.Pid_To_Integer(OS.Current_Process_Id)'Image, Left);
		found: Boolean := false;
	begin
		Create(output, Out_File, temp_name);
		Open(input, In_File, hist_name);
		begin
			while not End_Of_File(input) loop
				declare
					E: History_Entry := Next_Entry(input);
				begin
					if not found and then E.path = Path then
						E.time := Seconds_Since_Epoch;
						if E.count < Natural'Last then E.count := E.count + 1; end if;
						found := true;
					end if;
					Write(output, E);
				end;
			end loop;
		exception
			when End_Error => null;
		end;
		Close(input);
		if not found then
			Write(output, History_Entry'(Length => Path'Length, time => Seconds_Since_Epoch, count => 1, path => Path));
		end if;
		Close(output);
		Substitute(To => hist_name, From => temp_name);
	exception
		when Name_Error => Create_History(hist_name, Path);
		when E: others =>
			declare ok: Boolean; begin
				OS.Delete_File(temp_name, ok);
				if not ok and Exception_Name(E) /= "CONSTRAINT_ERROR" then Put_Line(Current_Error, "cascading lossage"); end if;
			end;
	end Bump_Count;

end History;
