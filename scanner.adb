with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Characters.Handling;
with Ada.Environment_Variables;
with Ada.Directories; use Ada.Directories;
with Ada.Exceptions; use Ada.Exceptions;

package body Scanner is

	debug_mode, ignore_case: Boolean := false;

	procedure Debug(Message: in String) is begin
		if debug_mode then Put_Line(Current_Error, Message); end if;
	end Debug;

	package Result is
		procedure Add(Path: in String);
		function To_Array return History.Path_Array;
	private
		subtype Chunk_Size is Natural range 0 .. 256;
		type Path_Chunk; type Chunk_Ptr is access all Path_Chunk;
		type Path_Chunk is
			record
				paths: History.Path_Array (1 .. Chunk_Size'Last);
				count: Chunk_Size;
				next: Chunk_Ptr;
			end record;
	end Result;

	package body Result is
		chunk: Chunk_Ptr := null;

		procedure Add(Path: in String) is
		begin
			if chunk = null or else chunk.count = Chunk_Size'Last then
				chunk := new Path_Chunk'(next => chunk, count => 0, paths => <>);
			end if;
			chunk.count := chunk.count + 1;
			chunk.paths(chunk.count) := To_Unbounded_String(Path);
		end Add;

		function To_Array return History.Path_Array is
			total: Natural := 0;
			cur: Chunk_Ptr := chunk;
		begin
			while cur /= null loop total := total + cur.count; cur := cur.next; end loop;
			return ret: History.Path_Array(1 .. total) do
				cur := chunk;
				while cur /= null loop
					pragma Assert (total >= cur.count);
					if cur.count > 0 then
						ret(total - cur.count + 1 .. total) := cur.paths(1 .. cur.count);
						total := total - cur.count;
					end if;
					cur := cur.next;
				end loop;
			end return;
		end To_Array;
	end Result;

	type Branch is
		record
			path, query: Unbounded_String;
		end record;

	package Queue is
		procedure Push(B: in Branch);
		function Pop(B: out Branch) return Boolean;
	private
		type Item; type Item_Ptr is access all Item;
		type Item is
			record
				next: Item_Ptr;
				B: Branch;
			end record;
	end Queue;

	package body Queue is
		trash, head: Item_Ptr := null;

		procedure Push(B: in Branch) is
			node: Item_Ptr := trash;
		begin
			if node /= null then
				trash := node.next;
				node.all := Item'(B => B, next => head);
			else
				node := new Item'(B => B, next => head);
			end if;
			head := node;
		end Push;

		function Pop(B: out Branch) return Boolean is
			got: Item_Ptr := head;
		begin
			if got = null then return false;
			else
				B := got.B;
				head := got.next;
				got.next := trash; trash := got;
				return true;
			end if;
		end Pop;
	end Queue;

	function Match_Length(Name: in String; Query: in Unbounded_String) return Natural is
		len: Natural := 0;
		use Ada.Characters.Handling;
		function Test(A, B: in Character) return Boolean is (if ignore_case then To_Lower(A) = To_Lower(B) else A = B);
	begin
		while (len < Name'Length and len < Length(Query)) and then Test(Name(Name'First + len), Element(Query, len + 1)) loop
			len := len + 1;
		end loop;
		if len > 0 then Debug("Name=`" & Name & "' matches Query=`" & To_String(Query) & "' for `" & Slice(Query, 1, len) & "'"); end if;
		return len;
	end Match_Length;

	function Ignore_Match(Pattern: in String; Path: in String) return Boolean is
		ign: String renames Pattern;
	begin
		if ign = Path then return true; end if;
		if ign'Length < Path'Length and then Path(Path'First .. Path'First + ign'Length - 1) = ign
			and then (ign(ign'Last) = '/' or Path(Path'First + ign'Length) = '/')
		then
			return true;
		end if;
		return false;
	end Ignore_Match;

	-- TODO: recognize slash query syntax
	function Scan(Path, Query: in Unbounded_String; First: out Branch; Opts: in Options.Option_Values) return Boolean is
		pragma Assert (not First.Path'Has_Same_Storage(Path));
		pragma Assert (not First.Query'Has_Same_Storage(Query));
		have_first: Boolean := false;
		procedure Scan_Item(Directory_Entry: in Directory_Entry_Type) is
			name: constant String := Simple_Name(Directory_Entry);
			function Ignored return Boolean is
				use Options;
				ign: Ignore_Ptr := Opts.ignore_list;
			begin
				if name = "." or name = ".." then return true; end if;
				declare full: constant String := Full_Name(Directory_Entry); begin
					while ign /= null loop
						if not Opts.no_ignore and then Ignore_Match(ign.path, full) then
							Debug("ignore pattern `" & ign.path & "' matched `" & full & "'");
							return true;
						end if;
						ign := ign.next;
					end loop;
				end;
				return false;
			end Ignored;
			match_len: Natural;
		begin
			if Kind(Directory_Entry) /= Directory then return; end if;
			match_len := Match_Length(name, Query);
			if match_len > 0 and then Ignored then return; end if;
			-- TODO: turn this loop into a match_len - 1 field in Branch,
			-- replace Path and Query with Branch, consider queries with
			-- 0..match_len - 1 prefixes dropped.
			for L in 1 .. match_len loop
				if Length(Query) = L then
					Result.Add(Full_Name(Directory_Entry));
					exit;
				end if;
				declare
					B: Branch := (path => To_Unbounded_String(Full_Name(Directory_Entry)), query => Tail(Query, Length(Query) - L));
				begin
					if have_first then Queue.Push(B);
					else
						First := B;
						have_first := true;
					end if;
				end;
			end loop;
		end Scan_Item;
	begin
		Debug("scanning `" & To_String(Path) & "'; query=`" & To_String(Query) & "'");
		Search(To_String(Path), "", Filter_Type'(others => true), Scan_Item'Access);
		return have_first;
	exception
		when E: others => Put_Line(Current_Error, Exception_Name(E) & ": " & Exception_Message(E)); raise;
	end Scan;

	package Env renames Ada.Environment_Variables;

	procedure Run(Opts: in Options.Option_Values; Query: in String) is
		item, next: Branch;
	begin
		debug_mode := Opts.verbose;
		ignore_case := Opts.icase;
		if Scan(To_Unbounded_String(Env.Value("HOME")), To_Unbounded_String(Query), item, Opts) then
			loop
				while Scan(item.path, item.query, next, Opts) loop item := next; end loop;
				exit when not Queue.Pop(item);
			end loop;
		end if;
		debug_mode := false; ignore_case := false;
	end Run;

	function Matches return History.Path_Array renames Result.To_Array;

end Scanner;
