generic
	type Index_Type is range <>;
	type Element_Type is private;
	type Array_Type is array (Index_Type range <>) of Element_Type;
	type Key_Type is private;
	with function "<" (Left: in Element_Type; Right: in Key_Type) return Boolean is <>;
	with function "=" (Left: in Element_Type; Right: in Key_Type) return Boolean is <>;
package Binary_Search is

	Not_Found: exception;
	function Binary_Search(Haystack: in Array_Type; Needle: in Key_Type) return Index_Type;

end Binary_Search;
