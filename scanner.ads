with Options, History;

package Scanner is

	procedure Run(Opts: in Options.Option_Values; Query: in String);
	function Matches return History.Path_Array;

end Scanner;
